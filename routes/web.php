<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//pages
Route::view('/contact', 'pages.contact');
Route::view('/cart', 'pages.shopping-cart');
Route::view('/wishlist', 'pages.wish-list');
Route::view('/catalogue', 'pages.catalogue');

//auth
Route::view('/login', 'auth.login');
Route::view('/register', 'auth.register');
