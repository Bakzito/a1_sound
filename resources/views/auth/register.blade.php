@extends('_layout.default')

@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="index.php?page=home">Home</a></li>
                    <li class='active'>Authentication</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content outer-top-bd">
        <div class="container">
            <div class="sign-in-page inner-bottom-sm">
                <div class="row">
                    <!-- Sign-in -->
                    <div class="col-md-3"></div>
                    <div class="col-md-6 col-sm-6 sign-in">
                        <h4 class="">sign Up</h4>

                        <form class="register-form outer-top-xs" role="form">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="info-title" for="exampleInputEmail1">Name
                                        <span>*</span></label>
                                    <input type="email" class="form-control unicase-form-control text-input"
                                           id="exampleInputEmail1">
                                </div>
                                <div class="col-md-6">
                                    <label class="info-title" for="exampleInputEmail1">Lastname
                                        <span>*</span></label>
                                    <input type="email" class="form-control unicase-form-control text-input"
                                           id="exampleInputEmail1">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label class="info-title" for="exampleInputEmail1">contact Number
                                        <span>*</span></label>
                                    <input type="email" class="form-control unicase-form-control text-input"
                                           id="exampleInputEmail1">
                                </div>
                                <div class="col-md-6">
                                    <label class="info-title" for="exampleInputEmail1">Email Address
                                        <span>*</span></label>
                                    <input type="email" class="form-control unicase-form-control text-input"
                                           id="exampleInputEmail1">
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label class="info-title" for="exampleInputEmail1">Password
                                        <span>*</span></label>
                                    <input type="email" class="form-control unicase-form-control text-input"
                                           id="exampleInputEmail1">
                                </div>
                                <div class="col-md-6">
                                    <label class="info-title" for="exampleInputEmail1">Confirm Password
                                        <span>*</span></label>
                                    <input type="email" class="form-control unicase-form-control text-input"
                                           id="exampleInputEmail1">
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn-upper btn btn-primary checkout-page-button mt-3">Sign up
                                </button>
                            </div>


                        </form>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>


@endsection
