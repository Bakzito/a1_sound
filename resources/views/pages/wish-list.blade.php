@extends('_layout.default')

@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="index.php?page=home">Home</a></li>
                    <li class='active'>Wishlish</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content outer-top-bd">
        <div class="container">
            <div class="my-wishlist-page inner-bottom-sm">
                <div class="row">
                    <div class="col-md-12 my-wishlist">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="4">my wishlist</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="col-md-2"><img src="assets/images/products/phone-1.png" alt="imga"></td>
                                    <td class="col-md-6">
                                        <div class="product-name"><a href="#">Simple Products Demo #1</a></div>
                                        <div class="rating">
                                            <i class="fa fa-star rate"></i>
                                            <i class="fa fa-star rate"></i>
                                            <i class="fa fa-star rate"></i>
                                            <i class="fa fa-star rate"></i>
                                            <i class="fa fa-star non-rate"></i>
                                            <span class="review">( 06 Reviews )</span>
                                        </div>
                                        <div class="price">
                                            R600.00
                                            <span>R900.00</span>
                                        </div>
                                    </td>
                                    <td class="col-md-2">
                                        <a href="#" class="btn-upper btn btn-primary">Add to cart</a>
                                    </td>
                                    <td class="col-md-2 close-btn">
                                        <a href="#" class=""><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-md-2"><img src="assets/images/products/phone-2.png" alt="phoro"></td>
                                    <td class="col-md-6">
                                        <div class="product-name"><a href="#">Simple Products Demo #2</a></div>
                                        <div class="rating">
                                            <i class="fa fa-star rate"></i>
                                            <i class="fa fa-star rate"></i>
                                            <i class="fa fa-star rate"></i>
                                            <i class="fa fa-star rate"></i>
                                            <i class="fa fa-star non-rate"></i>
                                            <span class="review">( 06 Reviews )</span>
                                        </div>
                                        <div class="price">
                                            R600.00
                                            <span>R900.00</span>
                                        </div>
                                    </td>
                                    <td class="col-md-2">
                                        <a href="#" class="btn-upper btn btn-default">Add to cart</a>
                                    </td>
                                    <td class="col-md-2 close-btn">
                                        <a href="#" class=""><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>			</div><!-- /.row -->
            </div><!-- /.sigin-in-->

@endsection
