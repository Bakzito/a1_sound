@extends('_layout.default')

@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li class='active'>Contact</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content outer-top-bd">
        <div class="container">
            <div class="row inner-bottom-sm">
                <div class="contact-page">
                    <div class="col-md-12 contact-map outer-bottom-vs">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3886.0080692193424!2d80.29172299999996!3d13.098675000000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a526f446a1c3187%3A0x298011b0b0d14d47!2sTransvelo!5e0!3m2!1sen!2sin!4v1412844527190"
                            width="600" height="450" style="border:0"></iframe>
                    </div>
                    <div class="col-md-9 contact-form">
                        <div class="col-md-12 contact-title">
                            <h4>Contact Form</h4>
                        </div>
                        <div class="col-md-4 ">
                            <form class="register-form" role="form">
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
                                    <input type="email" class="form-control unicase-form-control text-input"
                                           id="exampleInputName" placeholder="Name">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <form class="register-form" role="form">
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">Email Address
                                        <span>*</span></label>
                                    <input type="email" class="form-control unicase-form-control text-input"
                                           id="exampleInputEmail1" placeholder="admin@unicase.com">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <form class="register-form" role="form">
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputTitle">Title <span>*</span></label>
                                    <input type="email" class="form-control unicase-form-control text-input"
                                           id="exampleInputTitle" placeholder="Title">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-12">
                            <form class="register-form" role="form">
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputComments">Your Comments
                                        <span>*</span></label>
                                    <textarea class="form-control unicase-form-control"
                                              id="exampleInputComments"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-12 outer-bottom-small m-t-20">
                            <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Send Message
                            </button>
                        </div>
                    </div>
                    <div class="col-md-3 contact-info">
                        <div class="contact-title">
                            <h4>INFORMATION</h4>
                        </div>
                        <div class="clearfix address">
                            <span class="contact-i"><i class="fa fa-map-marker"></i></span>
                            <span class="contact-span">868 Any Stress, Burala Casi, Picasa USA.</span>
                        </div>
                        <div class="clearfix phone-no">
                            <span class="contact-i"><i class="fa fa-mobile"></i></span>
                            <span class="contact-span">(400) 0888 888 888 <br>(400) 888 848 868</span>
                        </div>
                        <div class="clearfix email">
                            <span class="contact-i"><i class="fa fa-envelope"></i></span>
                            <span class="contact-span"><a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                                          data-cfemail="12517d7c6673716652477c7b717361773c717d7f">[email&#160;protected]</a> <br><a
                                    href="/cdn-cgi/l/email-protection" class="__cf_email__"
                                    data-cfemail="4d1e2c21280d1823242e2c3e28632e2220">[email&#160;protected]</a></span>
                        </div>
                    </div>
                </div><!-- /.contact-page -->
            </div><!-- /.row -->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
            <div id="brands-carousel" class="logo-slider wow fadeInUp">

                <h3 class="section-title">Our Brands</h3>
                <div class="logo-slider-inner">
                    <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                        <div class="item m-t-15">
                            <a href="#" class="image">
                                <img data-echo="images/brands/ampeg-logo-130x100.jpg"
                                     src="images/brands/ampeg-logo-130x100.jpg" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item m-t-10">
                            <a href="#" class="image">
                                <img data-echo="images/brands/antari-logo-130x100.png"
                                     src="images/brands/antari-logo-130x100.png" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="images/brands/lewitt-logo-130x100.jpg"
                                     src="images/brands/lewitt-logo-130x100.jpg" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="images/brands/logo_AUDIOCENTER-130x100.png"
                                     src="images/brands/logo_AUDIOCENTER-130x100.png" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="images/brands/pioneer-130x100.gif"
                                     src="images/brands/pioneer-130x100.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="images/brands/stagg_logo-130x100.jpg"
                                     src="images/brands/stagg_logo-130x100.jpg" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="images/brands/yamaha-130x100.png" src="images/brands/yamaha-130x100.png"
                                     alt="">
                            </a>
                        </div><!--/.item-->
                    </div><!-- /.owl-carousel #logo-slider -->
                </div><!-- /.logo-slider-inner -->

            </div><!-- /.logo-slider -->
            <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
        </div>


@endsection
