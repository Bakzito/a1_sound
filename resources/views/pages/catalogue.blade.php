@extends('_layout.default')

@section('content')


    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li class='active'>Smart Phone</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content outer-top-xs">
        <div class="container">

    <div class="row outer-bottom-sm">
        <!-- ============================================== SIDEBAR ============================================== -->
        <div class="col-xs-12 col-sm-12 col-md-3 sidebar">

            <!-- ================================== TOP NAVIGATION ================================== -->
            <div class="side-menu animate-dropdown outer-bottom-xs">
                <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>
                <nav class="yamm megamenu-horizontal" role="navigation">
                    <ul class="nav">
                        <li class="dropdown menu-item">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="icon fa fa-desktop fa-fw"></i>Professional Sound</a>
                            <ul class="dropdown-menu mega-menu">
                                <li class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Accessories</a></li>
                                                <li><a href="index.php?page=category">Active Bassbin</a></li>
                                                <li><a href="index.php?page=category">Active Line Array</a></li>
                                                <li><a href="index.php?page=category">Active Speaker</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Active Stage Monitor</a>
                                                </li>
                                                <li><a href="index.php?page=category">Base Bins</a></li>
                                                <li><a href="index.php?page=category">Compression Drivers</a>
                                                </li>
                                                <li><a href="index.php?page=category">Compressor</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Conference Systems</a>
                                                </li>
                                                <li><a href="index.php?page=category">Crossover</a></li>
                                                <li><a href="index.php?page=category">Diaphrams</a></li>
                                                <li><a href="index.php?page=category">Digital Mixer</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Direct Box</a></li>
                                                <li><a href="index.php?page=category">Flight Cases & Bags</a>
                                                </li>
                                                <li><a href="index.php?page=category">Graphic Equalizer</a></li>
                                                <li><a href="index.php?page=category">Line Array</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Loose Speakers</a></li>
                                                <li><a href="index.php?page=category">Mixing Decks</a></li>
                                                <li><a href="index.php?page=category">Monitors</a></li>
                                                <li><a href="index.php?page=category">PA System / Combo</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Powered Mixer</a></li>
                                                <li><a href="index.php?page=category">Processors</a></li>
                                                <li><a href="index.php?page=category">Professional
                                                        Amplifiers</a></li>
                                                <li><a href="index.php?page=category">Recone Kits</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Snake Cables / Digital
                                                        Snake</a></li>
                                                <li><a href="index.php?page=category">Speaker Management</a>
                                                </li>
                                                <li><a href="index.php?page=category">Speakers</a></li>
                                                <li><a href="index.php?page=category">Show All Professional
                                                        Sound</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->
                                </li><!-- /.yamm-content -->
                            </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->

                        <li class="dropdown menu-item">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="icon fa fa-microphone fa-fw"></i>Studio Equipment</a>
                            <!-- ================================== MEGAMENU VERTICAL ================================== -->
                            <ul class="dropdown-menu mega-menu">
                                <li class="yamm-content">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-lg-4">
                                            <ul>
                                                <li><a href="#">Accessories</a></li>
                                                <li><a href="#">Headphone Amplifiers</a></li>
                                                <li><a href="#">Midi Controllers</a></li>
                                                <li><a href="#">Monitor Station</a></li>
                                                <li><a href="#">MPC / APC / MPD / LPD</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-lg-4">
                                            <ul>
                                                <li><a href="#">Pop shield</a></li>
                                                <li><a href="#">Reflection Filter</a></li>
                                                <li><a href="#">Rhythm Modules</a></li>
                                                <li><a href="#">Sound Cards/Midi Interface</a></li>
                                                <li><a href="#">Studio Headphones</a></li>
                                                <li><a href="#">Studio Software</a></li>
                                                <li><a href="#">Workstations</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-lg-4">
                                            <ul>
                                                <li><a href="#">Studio Microphones</a></li>
                                                <li><a href="#">Studio Mixers / DAW Controllers</a></li>
                                                <li><a href="#">Studio Monitors</a></li>
                                                <li><a href="#">SStudio Packages</a></li>
                                                <li><a href="#">Studio Processors</a></li>
                                                <li><a href="#">Show All Studio Equipment</a></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.row -->
                                </li><!-- /.yamm-content -->
                            </ul><!-- /.dropdown-menu -->
                            <!-- ================================== MEGAMENU VERTICAL ================================== -->
                        </li><!-- /.menu-item -->

                        <li class="dropdown menu-item">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="icon fa fa-apple fa-fw"></i>DJ Equipment</a>
                            <ul class="dropdown-menu mega-menu">
                                <li class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Cartridges</a></li>
                                                <li><a href="index.php?page=category">CD Players</a></li>
                                                <li><a href="index.php?page=category">DJ Accessories</a></li>
                                                <li><a href="index.php?page=category">DJ Controllers</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">DJ Headphones</a></li>
                                                <li><a href="index.php?page=category">DJ Mixers</a></li>
                                                <li><a href="index.php?page=category">DJ Packages</a></li>
                                                <li><a href="index.php?page=category">DJ Video Projectors</a>
                                                </li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Headshells</a></li>
                                                <li><a href="index.php?page=category">Interface</a></li>
                                                <li><a href="index.php?page=category">Needles</a></li>
                                                <li><a href="index.php?page=category">PA Combo Pack Deals</a>
                                                </li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Sample Pad</a></li>
                                                <li><a href="index.php?page=category">Serato DJ Controller
                                                        Series</a></li>
                                                <li><a href="index.php?page=category">Turntables</a></li>
                                                <li><a href="index.php?page=category">Show All DJ Equipment</a>
                                                </li>
                                            </ul>
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->
                                </li><!-- /.yamm-content -->
                            </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->

                        <li class="dropdown menu-item">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="icon fa fa-camera fa-fw"></i>Musical Equipment</a>
                            <ul class="dropdown-menu mega-menu">
                                <li class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Accessories</a></li>
                                                <li><a href="index.php?page=category">Acoustic Guitar</a></li>
                                                <li><a href="index.php?page=category">Amplifiers</a></li>
                                                <li><a href="index.php?page=category">Bass Combo</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Bass Guitar</a></li>
                                                <li><a href="index.php?page=category">Brass & Wind
                                                        Instruments</a></li>
                                                <li><a href="index.php?page=category">Celo</a></li>
                                                <li><a href="index.php?page=category">Conga</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Cymbals</a></li>
                                                <li><a href="index.php?page=category">Drum Accessories</a></li>
                                                <li><a href="index.php?page=category">Drums</a></li>
                                                <li><a href="index.php?page=category">Effects Pedal</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Electric Drums</a></li>
                                                <li><a href="index.php?page=category">Keyboard Amp</a></li>
                                                <li><a href="index.php?page=category">Keyboards & Pianos</a>
                                                </li>
                                                <li><a href="index.php?page=category">LEAD COMBO</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Lead Guitar</a></li>
                                                <li><a href="index.php?page=category">Percussions</a></li>
                                                <li><a href="index.php?page=category">Skins</a></li>
                                                <li><a href="index.php?page=category">String Instruments</a>
                                                </li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Synthesizer</a></li>
                                                <li><a href="index.php?page=category">Workstation</a></li>
                                                <li><a href="index.php?page=category">Show All Musical
                                                        Equipment</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->
                                </li><!-- /.yamm-content -->
                            </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->

                        <li class="dropdown menu-item">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="icon fa fa-headphones fa-fw"></i>Microphones</a>
                            <ul class="dropdown-menu mega-menu">
                                <li class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Combo Microphones</a></li>
                                                <li><a href="index.php?page=category">Condenser Microphone</a>
                                                </li>
                                                <li><a href="index.php?page=category">Corded Microphones</a>
                                                </li>
                                                <li><a href="index.php?page=category">Drum Microphone</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Handheld Cordless
                                                        Microphones</a></li>
                                                <li><a href="index.php?page=category">Handheld/Headset</a></li>
                                                <li><a href="index.php?page=category">Handheld/Lapel</a></li>
                                                <li><a href="index.php?page=category">Headsets</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Instrument Microphone</a>
                                                </li>
                                                <li><a href="index.php?page=category">Lapel Microphones</a></li>
                                                <li><a href="index.php?page=category">Microphone Accessories</a>
                                                </li>
                                                <li><a href="index.php?page=category">Receivers</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Shotgun Microphone</a>
                                                </li>
                                                <li><a href="index.php?page=category">Video Microphone</a></li>
                                                <li><a href="index.php?page=category">Wireless Camera
                                                        Microphone</a></li>
                                                <li><a href="index.php?page=category">Wireless In-Ear
                                                        Monitors</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Wireless Microphone</a>
                                                </li>
                                                <li><a href="index.php?page=category">Show All Microphones</a>
                                                </li>
                                            </ul>
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->
                                </li><!-- /.yamm-content -->
                            </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->

                        <li class="dropdown menu-item">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="icon fa fa-gamepad fa-fw"></i>Professional Lighting</a>
                            <ul class="dropdown-menu mega-menu">
                                <li class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Controllers</a></li>
                                                <li><a href="index.php?page=category">Dimmer Packs</a></li>
                                                <li><a href="index.php?page=category">Effect Lights</a></li>
                                                <li><a href="index.php?page=category">Flame Machine</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Fluid & Liquid</a></li>
                                                <li><a href="index.php?page=category">Foam Machine</a></li>
                                                <li><a href="index.php?page=category">Fog & Bubbles</a></li>
                                                <li><a href="index.php?page=category">Follow Spots</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Haze Machine</a></li>
                                                <li><a href="index.php?page=category">Lasers</a></li>
                                                <li><a href="index.php?page=category">LED Lights</a></li>
                                                <li><a href="index.php?page=category">Light Controller</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Light Packages</a></li>
                                                <li><a href="index.php?page=category">Mirror Balls</a></li>
                                                <li><a href="index.php?page=category">Moving Heads</a></li>
                                                <li><a href="index.php?page=category">Par Cans</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Scanners</a></li>
                                                <li><a href="index.php?page=category">Smoke Machine</a></li>
                                                <li><a href="index.php?page=category">Smoke Machine</a></li>
                                                <li><a href="index.php?page=category">Software</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Stage Washes</a></li>
                                                <li><a href="index.php?page=category">Stands & Acc</a></li>
                                                <li><a href="index.php?page=category">Strobe Lights</a></li>
                                                <li><a href="index.php?page=category">UV Lights</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Show All Professional
                                                        Lighting</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->
                                </li><!-- /.yamm-content -->
                            </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->

                        <li class="dropdown menu-item">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                    class="icon fa fa-location-arrow fa-fw"></i>Other</a>
                            <ul class="dropdown-menu mega-menu">
                                <li class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Bags</a></li>
                                                <li><a href="index.php?page=category">Bluetooth Headsets</a>
                                                </li>
                                                <li><a href="index.php?page=category">Cabling & Accessories</a>
                                                </li>
                                                <li><a href="index.php?page=category">Camera Microphones</a>
                                                </li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Projectors</a></li>
                                                <li><a href="index.php?page=category">Samplers</a></li>
                                                <li><a href="index.php?page=category">Sound Module</a></li>
                                                <li><a href="index.php?page=category">Stands</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <li><a href="index.php?page=category">Trussing & Stage</a></li>
                                                <li><a href="index.php?page=category">Video Mixers</a></li>
                                                <li><a href="index.php?page=category">Walkie Talkie</a></li>
                                            </ul>
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->
                                </li><!-- /.yamm-content -->
                            </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->


                    </ul><!-- /.nav -->
                </nav><!-- /.megamenu-horizontal -->
            </div><!-- /.side-menu -->
            <!-- ================================== TOP NAVIGATION : END ================================== -->
            <!-- ============================================== SPECIAL OFFER ============================================== -->

            <div class="sidebar-widget outer-bottom-small wow fadeInUp">
                <h3 class="section-title">Special Offer</h3>
                <div class="sidebar-widget-body outer-top-xs">
                    <div
                        class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
                        <div class="item">
                            <div class="products special-product">
                                <div class="product">
                                    <div class="product-micro">
                                        <div class="row product-micro-row">
                                            <div class="col col-xs-5">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="images/products/amplifiers/CPD3600-200x200.jpg"
                                                           data-lightbox="image-1" data-title="Nunc ullamcors">
                                                            <img style="width: 130%"
                                                                 data-echo="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                 src="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                 alt="">
                                                            <div class="zoom-overlay"></div>
                                                        </a>
                                                    </div><!-- /.image -->
                                                    <div class="tag tag-micro hot">
                                                        <span>hot</span>
                                                    </div>


                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-xs-7">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="#">Amplifiers</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                R650.99				</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                            To Cart</a></div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div><!-- /.product-micro-row -->
                                    </div><!-- /.product-micro -->

                                </div>
                                <div class="product">
                                    <div class="product-micro">
                                        <div class="row product-micro-row">
                                            <div class="col col-xs-5">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="images/products/drums/NITRO-200x200.png"
                                                           data-lightbox="image-1" data-title="Nunc ullamcors">
                                                            <img style="width: 130%"
                                                                 data-echo="images/products/drums/NITRO-200x200.png"
                                                                 src="images/products/drums/NITRO-200x200.png" alt="">
                                                            <div class="zoom-overlay"></div>
                                                        </a>
                                                    </div><!-- /.image -->


                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-xs-7">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="#">Drums</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                            To Cart</a></div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div><!-- /.product-micro-row -->
                                    </div><!-- /.product-micro -->

                                </div>
                                <div class="product">
                                    <div class="product-micro">
                                        <div class="row product-micro-row">
                                            <div class="col col-xs-5">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                           data-lightbox="image-1" data-title="Nunc ullamcors">
                                                            <img style="width: 130%"
                                                                 data-echo="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                 src="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                 alt="">
                                                            <div class="zoom-overlay"></div>
                                                        </a>
                                                    </div><!-- /.image -->

                                                    <div class="tag tag-micro new">
                                                        <span>new</span>
                                                    </div>

                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-xs-7">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="#">Headpones</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                            To Cart</a></div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div><!-- /.product-micro-row -->
                                    </div><!-- /.product-micro -->

                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="products special-product">
                                <div class="product">
                                    <div class="product-micro">
                                        <div class="row product-micro-row">
                                            <div class="col col-xs-5">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="images/products/amplifiers/CPD3600-200x200.jpg"
                                                           data-lightbox="image-1" data-title="Nunc ullamcors">
                                                            <img style="width: 130%"
                                                                 data-echo="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                 src="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                 alt="">
                                                            <div class="zoom-overlay"></div>
                                                        </a>
                                                    </div><!-- /.image -->
                                                    <div class="tag tag-micro hot">
                                                        <span>hot</span>
                                                    </div>


                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-xs-7">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="#">Amplifiers</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                R650.99				</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                            To Cart</a></div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div><!-- /.product-micro-row -->
                                    </div><!-- /.product-micro -->

                                </div>
                                <div class="product">
                                    <div class="product-micro">
                                        <div class="row product-micro-row">
                                            <div class="col col-xs-5">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="images/products/drums/NITRO-200x200.png"
                                                           data-lightbox="image-1" data-title="Nunc ullamcors">
                                                            <img style="width: 130%"
                                                                 data-echo="images/products/drums/NITRO-200x200.png"
                                                                 src="images/products/drums/NITRO-200x200.png" alt="">
                                                            <div class="zoom-overlay"></div>
                                                        </a>
                                                    </div><!-- /.image -->


                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-xs-7">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="#">Drums</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                            To Cart</a></div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div><!-- /.product-micro-row -->
                                    </div><!-- /.product-micro -->

                                </div>
                                <div class="product">
                                    <div class="product-micro">
                                        <div class="row product-micro-row">
                                            <div class="col col-xs-5">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                           data-lightbox="image-1" data-title="Nunc ullamcors">
                                                            <img style="width: 130%"
                                                                 data-echo="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                 src="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                 alt="">
                                                            <div class="zoom-overlay"></div>
                                                        </a>
                                                    </div><!-- /.image -->

                                                    <div class="tag tag-micro new">
                                                        <span>new</span>
                                                    </div>

                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-xs-7">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="#">Headpones</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                            To Cart</a></div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div><!-- /.product-micro-row -->
                                    </div><!-- /.product-micro -->

                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="products special-product">
                                <div class="product">
                                    <div class="product-micro">
                                        <div class="row product-micro-row">
                                            <div class="col col-xs-5">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="images/products/amplifiers/CPD3600-200x200.jpg"
                                                           data-lightbox="image-1" data-title="Nunc ullamcors">
                                                            <img style="width: 130%"
                                                                 data-echo="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                 src="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                 alt="">
                                                            <div class="zoom-overlay"></div>
                                                        </a>
                                                    </div><!-- /.image -->
                                                    <div class="tag tag-micro hot">
                                                        <span>hot</span>
                                                    </div>


                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-xs-7">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="#">Amplifiers</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
                                                            <span class="price">
                                                                R650.99				</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                            To Cart</a></div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div><!-- /.product-micro-row -->
                                    </div><!-- /.product-micro -->

                                </div>
                                <div class="product">
                                    <div class="product-micro">
                                        <div class="row product-micro-row">
                                            <div class="col col-xs-5">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="images/products/drums/NITRO-200x200.png"
                                                           data-lightbox="image-1" data-title="Nunc ullamcors">
                                                            <img style="width: 130%"
                                                                 data-echo="images/products/drums/NITRO-200x200.png"
                                                                 src="images/products/drums/NITRO-200x200.png" alt="">
                                                            <div class="zoom-overlay"></div>
                                                        </a>
                                                    </div><!-- /.image -->


                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-xs-7">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="#">Drums</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                            To Cart</a></div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div><!-- /.product-micro-row -->
                                    </div><!-- /.product-micro -->

                                </div>
                                <div class="product">
                                    <div class="product-micro">
                                        <div class="row product-micro-row">
                                            <div class="col col-xs-5">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                           data-lightbox="image-1" data-title="Nunc ullamcors">
                                                            <img style="width: 130%"
                                                                 data-echo="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                 src="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                 alt="">
                                                            <div class="zoom-overlay"></div>
                                                        </a>
                                                    </div><!-- /.image -->

                                                    <div class="tag tag-micro new">
                                                        <span>new</span>
                                                    </div>

                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-xs-7">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="#">Headpones</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                            To Cart</a></div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div><!-- /.product-micro-row -->
                                    </div><!-- /.product-micro -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.sidebar-widget-body -->
            </div><!-- /.sidebar-widget -->
            <!-- ============================================== SPECIAL OFFER : END ============================================== -->
            <!-- ============================================== PRODUCT TAGS ============================================== -->
            <div class="sidebar-widget product-tag wow fadeInUp">
                <h3 class="section-title">Product tags</h3>
                <div class="sidebar-widget-body outer-top-xs">
                    <div class="tag-list">
                        <a class="item" title="Phone" href="index.php?page=category">Sound</a>
                        <a class="item active" title="Vest" href="index.php?page=category">Studio</a>
                        <a class="item" title="Smartphone" href="index.php?page=category">DJ</a>
                        <a class="item" title="Furniture" href="index.php?page=category">Musical</a>
                        <a class="item" title="T-shirt" href="index.php?page=category">Microphones</a>
                        <a class="item" title="Sweatpants" href="index.php?page=category">Lighting</a>
                        <a class="item" title="Sneaker" href="index.php?page=category">Other</a>
                    </div><!-- /.tag-list -->
                </div><!-- /.sidebar-widget-body -->
            </div><!-- /.sidebar-widget -->
            <!-- ============================================== PRODUCT TAGS : END ============================================== -->
            <!-- ============================================== SPECIAL DEALS ============================================== -->

            <!-- /.sidebar-widget -->
            <!-- ============================================== SPECIAL DEALS : END ============================================== -->
            <!-- ============================================== NEWSLETTER ============================================== -->
            <div class="sidebar-widget newsletter wow fadeInUp outer-bottom-small">
                <h3 class="section-title">Newsletters</h3>
                <div class="sidebar-widget-body outer-top-xs">
                    <p>Sign Up for Our Newsletter!</p>
                    <form role="form">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1"
                                   placeholder="Subscribe to our newsletter">
                        </div>
                        <button class="btn btn-primary">Subscribe</button>
                    </form>
                </div><!-- /.sidebar-widget-body -->
            </div><!-- /.sidebar-widget -->
            <!-- ============================================== NEWSLETTER: END ============================================== -->
            <!-- ============================================== HOT DEALS ============================================== -->
            <div class="sidebar-widget hot-deals wow fadeInUp">
                <h3 class="section-title">hot deals</h3>
                <div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-xs">

                    <div class="item">
                        <div class="products">
                            <div class="hot-deal-wrapper">
                                <div class="image">
                                    <img src="images/products/speakers/HYBRID+ HP218_(Front)_web-228x228.png" alt="">
                                </div>
                                <div class="sale-offer-tag"><span>35%<br>off</span></div>
                                <div class="timing-wrapper">
                                    <div class="box-wrapper">
                                        <div class="date box">
                                            <span class="key">120</span>
                                            <span class="value">Days</span>
                                        </div>
                                    </div>

                                    <div class="box-wrapper">
                                        <div class="hour box">
                                            <span class="key">20</span>
                                            <span class="value">HRS</span>
                                        </div>
                                    </div>

                                    <div class="box-wrapper">
                                        <div class="minutes box">
                                            <span class="key">36</span>
                                            <span class="value">MINS</span>
                                        </div>
                                    </div>

                                    <div class="box-wrapper hidden-md">
                                        <div class="seconds box">
                                            <span class="key">60</span>
                                            <span class="value">SEC</span>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.hot-deal-wrapper -->

                            <div class="product-info text-left m-t-20">
                                <h3 class="name"><a href="index.php?page=detail">HYBRID + HP218 DUAL 18" SUB 2200 WATT</a>
                                </h3>
                                <div class="rating rateit-small"></div>

                                <div class="product-price">
								<span class="price">
									R600.00
								</span>

                                    <span class="price-before-discount">R800.00</span>

                                </div><!-- /.product-price -->

                            </div><!-- /.product-info -->

                            <div class="cart clearfix animate-effect">
                                <div class="action">

                                    <div class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown"
                                                type="button">
                                            <i class="fa fa-shopping-cart"></i>
                                        </button>
                                        <button class="btn btn-primary" type="button">Add to cart</button>

                                    </div>

                                </div><!-- /.action -->
                            </div><!-- /.cart -->
                        </div>
                    </div>
                    <div class="item">
                        <div class="products">
                            <div class="hot-deal-wrapper">
                                <div class="image">
                                    <img src="images/products/other/apc40_web_large_700x438-228x228.png" alt="">
                                </div>
                                <div class="sale-offer-tag"><span>35%<br>off</span></div>
                                <div class="timing-wrapper">
                                    <div class="box-wrapper">
                                        <div class="date box">
                                            <span class="key">120</span>
                                            <span class="value">Days</span>
                                        </div>
                                    </div>

                                    <div class="box-wrapper">
                                        <div class="hour box">
                                            <span class="key">20</span>
                                            <span class="value">HRS</span>
                                        </div>
                                    </div>

                                    <div class="box-wrapper">
                                        <div class="minutes box">
                                            <span class="key">36</span>
                                            <span class="value">MINS</span>
                                        </div>
                                    </div>

                                    <div class="box-wrapper hidden-md">
                                        <div class="seconds box">
                                            <span class="key">60</span>
                                            <span class="value">SEC</span>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.hot-deal-wrapper -->

                            <div class="product-info text-left m-t-20">
                                <h3 class="name"><a href="index.php?page=detail">Akai APC40 Ableton Performance
                                        Controller</a>
                                </h3>
                                <div class="rating rateit-small"></div>

                                <div class="product-price">
								<span class="price">
									R600.00
								</span>

                                    <span class="price-before-discount">R800.00</span>

                                </div><!-- /.product-price -->

                            </div><!-- /.product-info -->

                            <div class="cart clearfix animate-effect">
                                <div class="action">

                                    <div class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown"
                                                type="button">
                                            <i class="fa fa-shopping-cart"></i>
                                        </button>
                                        <button class="btn btn-primary" type="button">Add to cart</button>

                                    </div>

                                </div><!-- /.action -->
                            </div><!-- /.cart -->
                        </div>
                    </div>
                    <div class="item">
                        <div class="products">
                            <div class="hot-deal-wrapper">
                                <div class="image">
                                    <img src="images/products/other/wk240_1bigger-228x228.jpg" alt="">
                                </div>
                                <div class="sale-offer-tag"><span>35%<br>off</span></div>
                                <div class="timing-wrapper">
                                    <div class="box-wrapper">
                                        <div class="date box">
                                            <span class="key">120</span>
                                            <span class="value">Days</span>
                                        </div>
                                    </div>

                                    <div class="box-wrapper">
                                        <div class="hour box">
                                            <span class="key">20</span>
                                            <span class="value">HRS</span>
                                        </div>
                                    </div>

                                    <div class="box-wrapper">
                                        <div class="minutes box">
                                            <span class="key">36</span>
                                            <span class="value">MINS</span>
                                        </div>
                                    </div>

                                    <div class="box-wrapper hidden-md">
                                        <div class="seconds box">
                                            <span class="key">60</span>
                                            <span class="value">SEC</span>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.hot-deal-wrapper -->

                            <div class="product-info text-left m-t-20">
                                <h3 class="name"><a href="index.php?page=detail">RODE NTG3B SHOTGUN CONDENSOR MICROPHONE</a>
                                </h3>
                                <div class="rating rateit-small"></div>

                                <div class="product-price">
								<span class="price">
									R600.00
								</span>

                                    <span class="price-before-discount">R800.00</span>

                                </div><!-- /.product-price -->

                            </div><!-- /.product-info -->

                            <div class="cart clearfix animate-effect">
                                <div class="action">

                                    <div class="add-cart-button btn-group">
                                        <button class="btn btn-primary icon" data-toggle="dropdown"
                                                type="button">
                                            <i class="fa fa-shopping-cart"></i>
                                        </button>
                                        <button class="btn btn-primary" type="button">Add to cart</button>

                                    </div>

                                </div><!-- /.action -->
                            </div><!-- /.cart -->
                        </div>
                    </div>


                </div><!-- /.sidebar-widget -->
            </div>
            <!-- ============================================== HOT DEALS: END ============================================== -->
            <!-- ============================================== COLOR============================================== -->
        <!-- ============================================== COLOR: END ============================================== -->


        </div><!-- /.sidemenu-holder -->
        <!-- ============================================== SIDEBAR : END ============================================== -->
        <div class='col-md-9'>
            <!-- ========================================== SECTION – HERO ========================================= -->

            <div id="category" class="category-carousel hidden-xs">
                <div class="item">
                    <div class="image">
                        <img src="images/products/banners/Jbl-speakers-banner_edited-1.jpg" alt="" class="img-responsive">
                    </div>
                    <div class="container-fluid">
                        <div class="caption vertical-top text-left">
                            <div class="big-text">
                                Sale
                            </div>

                            <div class="excerpt hidden-sm hidden-md">
                                up to 50% off
                            </div>

                        </div><!-- /.caption -->
                    </div><!-- /.container-fluid -->
                </div>
            </div>


            <!-- ========================================= SECTION – HERO : END ========================================= -->
            <div class="clearfix filters-container m-t-10">
                <div class="row">
                    <div class="col col-sm-6 col-md-2">
                        <div class="filter-tabs">
                            <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                                <li class="active">
                                    <a data-toggle="tab" href="#grid-container"><i class="icon fa fa-th-list"></i>Grid</a>
                                </li>
                                <li><a data-toggle="tab" href="#list-container"><i class="icon fa fa-th"></i>List</a></li>
                            </ul>
                        </div><!-- /.filter-tabs -->
                    </div><!-- /.col -->
                    <div class="col col-sm-12 col-md-6">
                        <div class="col col-sm-3 col-md-6 no-padding">
                            <div class="lbl-cnt">
                                <span class="lbl">Sort by</span>
                                <div class="fld inline">
                                    <div class="dropdown dropdown-small dropdown-med dropdown-white inline">
                                        <button data-toggle="dropdown" type="button" class="btn dropdown-toggle">
                                            Position <span class="caret"></span>
                                        </button>

                                        <ul role="menu" class="dropdown-menu">
                                            <li role="presentation"><a href="#">position</a></li>
                                            <li role="presentation"><a href="#">Price:Lowest first</a></li>
                                            <li role="presentation"><a href="#">Price:HIghest first</a></li>
                                            <li role="presentation"><a href="#">Product Name:A to Z</a></li>
                                        </ul>
                                    </div>
                                </div><!-- /.fld -->
                            </div><!-- /.lbl-cnt -->
                        </div><!-- /.col -->
                        <div class="col col-sm-3 col-md-6 no-padding">
                            <div class="lbl-cnt">
                                <span class="lbl">Show</span>
                                <div class="fld inline">
                                    <div class="dropdown dropdown-small dropdown-med dropdown-white inline">
                                        <button data-toggle="dropdown" type="button" class="btn dropdown-toggle">
                                            1 <span class="caret"></span>
                                        </button>

                                        <ul role="menu" class="dropdown-menu">
                                            <li role="presentation"><a href="#">1</a></li>
                                            <li role="presentation"><a href="#">2</a></li>
                                            <li role="presentation"><a href="#">3</a></li>
                                            <li role="presentation"><a href="#">4</a></li>
                                            <li role="presentation"><a href="#">5</a></li>
                                            <li role="presentation"><a href="#">6</a></li>
                                            <li role="presentation"><a href="#">7</a></li>
                                            <li role="presentation"><a href="#">8</a></li>
                                            <li role="presentation"><a href="#">9</a></li>
                                            <li role="presentation"><a href="#">10</a></li>
                                        </ul>
                                    </div>
                                </div><!-- /.fld -->
                            </div><!-- /.lbl-cnt -->
                        </div><!-- /.col -->
                    </div><!-- /.col -->
                    <div class="col col-sm-6 col-md-4 text-right">
                        <div class="pagination-container">
                            <ul class="list-inline list-unstyled">
                                <li class="prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                <li><a href="#">1</a></li>
                                <li class="active"><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li class="next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul><!-- /.list-inline -->
                        </div><!-- /.pagination-container -->        </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
            <div class="search-result-container">
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane active " id="grid-container">
                        <div class="category-product  inner-top-vs">
                            <div class="row">

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products/amplifiers/Code100C-large-200x200.jpg"
                                                                                         data-echo="images/products/amplifiers/Code100C-large-200x200.jpg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag new"><span>new</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Sony Ericson</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                                         data-echo="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag sale"><span>sale</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products/amplifiers/ep4000-200x200.jpg"
                                                                                         data-echo="images/products/amplifiers/ep4000-200x200.jpg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag hot"><span>hot</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products/amplifiers/VA1201_web-300x300-200x200.jpg"
                                                                                         data-echo="images/products/amplifiers/VA1201_web-300x300-200x200.jpg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag hot"><span>hot</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Apple Iphone 5s 32GB</a>
                                                </h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products/drums/NITRO-200x200.png"
                                                                                         data-echo="images/products/drums/NITRO-200x200.png"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag sale"><span>sale</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Nokia</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products/drums/ROADSHOW-200x200.jpeg"
                                                                                         data-echo="images/products/drums/ROADSHOW-200x200.jpeg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag new"><span>new</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Nokia</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products//drums/tama-200x200.jpg"
                                                                                         data-echo="images/products/drums/tama-200x200.jpg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag new"><span>new</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Sony Ericson Vaga</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products//drums/Yamaha_DD65-200x200.jpg"
                                                                                         data-echo="images/products/drums/Yamaha_DD65-200x200.jpg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag sale"><span>sale</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                                         data-echo="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag hot"><span>hot</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products/headphones/HDJ-1500-200x200.jpg"
                                                                                         data-echo="images/products/headphones/HDJ-1500-200x200.jpg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag new"><span>new</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Sony Ericson Vaga</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products/headphones/HP1100-200x200.jpg"
                                                                                         data-echo="images/products/headphones/HP1100-200x200.jpg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag sale"><span>sale</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->

                                <div class="col-sm-6 col-md-4 wow fadeInUp">
                                    <div class="products">

                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">
                                                    <a href="index.php?page=detail"><img src="images/products/headphones/RPDJ1200-200x200.jpg"
                                                                                         data-echo="images/products/headphones/RPDJ1200-200x200.jpg"
                                                                                         alt=""></a>
                                                </div><!-- /.image -->

                                                <div class="tag hot"><span>hot</span></div>
                                            </div><!-- /.product-image -->


                                            <div class="product-info text-left">
                                                <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>

                                                <div class="product-price">
				<span class="price">
					$650.99				</span>
                                                    <span class="price-before-discount">$ 800</span>

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary" type="button">Add to cart
                                                            </button>

                                                        </li>

                                                        <li class="lnk wishlist">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Wishlist">
                                                                <i class="icon fa fa-heart"></i>
                                                            </a>
                                                        </li>

                                                        <li class="lnk">
                                                            <a class="add-to-cart" href="index.php?page=detail"
                                                               title="Compare">
                                                                <i class="fa fa-retweet"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div><!-- /.product -->

                                    </div><!-- /.products -->
                                </div><!-- /.item -->
                            </div><!-- /.row -->
                        </div><!-- /.category-product -->

                    </div><!-- /.tab-pane -->

                    <div class="tab-pane " id="list-container">
                        <div class="category-product  inner-top-vs">


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c1.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Simple Products Demo
                                                            Showcase</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag new"><span>new</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c2.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a>
                                                    </h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag sale"><span>sale</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c3.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a>
                                                    </h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag hot"><span>hot</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c4.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Nokia</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag hot"><span>hot</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c5.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Apple Iphone 5s
                                                            32GB</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag sale"><span>sale</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c6.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Nokia</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag new"><span>new</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c5.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Sony Ericson Vaga</a>
                                                    </h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag new"><span>new</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c3.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a>
                                                    </h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag sale"><span>sale</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c1.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a>
                                                    </h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag hot"><span>hot</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c2.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Sony Ericson Vaga</a>
                                                    </h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag new"><span>new</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c6.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a>
                                                    </h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag sale"><span>sale</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                            <div class="category-product-inner wow fadeInUp">
                                <div class="products">
                                    <div class="product-list product">
                                        <div class="row product-list-row">
                                            <div class="col col-sm-4 col-lg-4">
                                                <div class="product-image">
                                                    <div class="image">
                                                        <img data-echo="images/products/products/c3.jpg"
                                                             src="images/products/blank.gif" alt="">
                                                    </div>
                                                </div><!-- /.product-image -->
                                            </div><!-- /.col -->
                                            <div class="col col-sm-8 col-lg-8">
                                                <div class="product-info">
                                                    <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy S4</a>
                                                    </h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="product-price">
					<span class="price">
						$650.99					</span>
                                                        <span class="price-before-discount">$ 800</span>

                                                    </div><!-- /.product-price -->
                                                    <div class="description m-t-10">Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id
                                                        accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum
                                                        gravida eget.
                                                    </div>
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->

                                                </div><!-- /.product-info -->
                                            </div><!-- /.col -->
                                        </div><!-- /.product-list-row -->
                                        <div class="tag hot"><span>hot</span></div>
                                    </div><!-- /.product-list -->
                                </div><!-- /.products -->
                            </div><!-- /.category-product-inner -->


                        </div><!-- /.category-product -->
                    </div><!-- /.tab-pane #list-container -->
                </div><!-- /.tab-content -->
                <div class="clearfix filters-container">

                    <div class="text-right">
                        <div class="pagination-container">
                            <ul class="list-inline list-unstyled">
                                <li class="prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                <li><a href="#">1</a></li>
                                <li class="active"><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li class="next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul><!-- /.list-inline -->
                        </div><!-- /.pagination-container -->                            </div><!-- /.text-right -->

                </div><!-- /.filters-container -->

            </div><!-- /.search-result-container -->

        </div><!-- /.col -->


    </div><!-- /.row -->



    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">

        <h3 class="section-title">Our Brands</h3>
        <div class="logo-slider-inner">
            <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                <div class="item m-t-15">
                    <a href="#" class="image">
                        <img data-echo="images/brands/ampeg-logo-130x100.jpg"
                             src="images/brands/ampeg-logo-130x100.jpg" alt="">
                    </a>
                </div><!--/.item-->

                <div class="item m-t-10">
                    <a href="#" class="image">
                        <img data-echo="images/brands/antari-logo-130x100.png"
                             src="images/brands/antari-logo-130x100.png" alt="">
                    </a>
                </div><!--/.item-->

                <div class="item">
                    <a href="#" class="image">
                        <img data-echo="images/brands/lewitt-logo-130x100.jpg"
                             src="images/brands/lewitt-logo-130x100.jpg" alt="">
                    </a>
                </div><!--/.item-->

                <div class="item">
                    <a href="#" class="image">
                        <img data-echo="images/brands/logo_AUDIOCENTER-130x100.png"
                             src="images/brands/logo_AUDIOCENTER-130x100.png" alt="">
                    </a>
                </div><!--/.item-->

                <div class="item">
                    <a href="#" class="image">
                        <img data-echo="images/brands/pioneer-130x100.gif"
                             src="images/brands/pioneer-130x100.gif" alt="">
                    </a>
                </div><!--/.item-->

                <div class="item">
                    <a href="#" class="image">
                        <img data-echo="images/brands/stagg_logo-130x100.jpg"
                             src="images/brands/stagg_logo-130x100.jpg" alt="">
                    </a>
                </div><!--/.item-->

                <div class="item">
                    <a href="#" class="image">
                        <img data-echo="images/brands/yamaha-130x100.png" src="images/brands/yamaha-130x100.png"
                             alt="">
                    </a>
                </div><!--/.item-->
            </div><!-- /.owl-carousel #logo-slider -->
        </div><!-- /.logo-slider-inner -->

    </div><!-- /.logo-slider -->
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->


    </div><!-- /.container -->

    </div><!-- /.body-content -->

@endsection
