@extends('_layout.default')

@section('content')

    <div class="body-content outer-top-xs" id="top-banner-and-menu">
        <div class="container">
            <div class="row">
                <!-- ============================================== SIDEBAR ============================================== -->
                <div class="col-xs-12 col-sm-12 col-md-3 sidebar">

                    <!-- ================================== TOP NAVIGATION ================================== -->
                    <div class="side-menu animate-dropdown outer-bottom-xs">
                        <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>
                        <nav class="yamm megamenu-horizontal" role="navigation">
                            <ul class="nav">
                                <li class="dropdown menu-item">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon fa fa-desktop fa-fw"></i>Professional Sound</a>
                                    <ul class="dropdown-menu mega-menu">
                                        <li class="yamm-content">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="/catalogue">Accessories</a></li>
                                                        <li><a href="index.php?page=category">Active Bassbin</a></li>
                                                        <li><a href="index.php?page=category">Active Line Array</a></li>
                                                        <li><a href="index.php?page=category">Active Speaker</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Active Stage Monitor</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Base Bins</a></li>
                                                        <li><a href="index.php?page=category">Compression Drivers</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Compressor</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Conference Systems</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Crossover</a></li>
                                                        <li><a href="index.php?page=category">Diaphrams</a></li>
                                                        <li><a href="index.php?page=category">Digital Mixer</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Direct Box</a></li>
                                                        <li><a href="index.php?page=category">Flight Cases & Bags</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Graphic Equalizer</a></li>
                                                        <li><a href="index.php?page=category">Line Array</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Loose Speakers</a></li>
                                                        <li><a href="index.php?page=category">Mixing Decks</a></li>
                                                        <li><a href="index.php?page=category">Monitors</a></li>
                                                        <li><a href="index.php?page=category">PA System / Combo</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Powered Mixer</a></li>
                                                        <li><a href="index.php?page=category">Processors</a></li>
                                                        <li><a href="index.php?page=category">Professional
                                                                Amplifiers</a></li>
                                                        <li><a href="index.php?page=category">Recone Kits</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Snake Cables / Digital
                                                                Snake</a></li>
                                                        <li><a href="index.php?page=category">Speaker Management</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Speakers</a></li>
                                                        <li><a href="index.php?page=category">Show All Professional
                                                                Sound</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                            </div><!-- /.row -->
                                        </li><!-- /.yamm-content -->
                                    </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->

                                <li class="dropdown menu-item">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon fa fa-microphone fa-fw"></i>Studio Equipment</a>
                                    <!-- ================================== MEGAMENU VERTICAL ================================== -->
                                    <ul class="dropdown-menu mega-menu">
                                        <li class="yamm-content">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-lg-4">
                                                    <ul>
                                                        <li><a href="#">Accessories</a></li>
                                                        <li><a href="#">Headphone Amplifiers</a></li>
                                                        <li><a href="#">Midi Controllers</a></li>
                                                        <li><a href="#">Monitor Station</a></li>
                                                        <li><a href="#">MPC / APC / MPD / LPD</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-lg-4">
                                                    <ul>
                                                        <li><a href="#">Pop shield</a></li>
                                                        <li><a href="#">Reflection Filter</a></li>
                                                        <li><a href="#">Rhythm Modules</a></li>
                                                        <li><a href="#">Sound Cards/Midi Interface</a></li>
                                                        <li><a href="#">Studio Headphones</a></li>
                                                        <li><a href="#">Studio Software</a></li>
                                                        <li><a href="#">Workstations</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-lg-4">
                                                    <ul>
                                                        <li><a href="#">Studio Microphones</a></li>
                                                        <li><a href="#">Studio Mixers / DAW Controllers</a></li>
                                                        <li><a href="#">Studio Monitors</a></li>
                                                        <li><a href="#">SStudio Packages</a></li>
                                                        <li><a href="#">Studio Processors</a></li>
                                                        <li><a href="#">Show All Studio Equipment</a></li>
                                                    </ul>
                                                </div>
                                            </div><!-- /.row -->
                                        </li><!-- /.yamm-content -->
                                    </ul><!-- /.dropdown-menu -->
                                    <!-- ================================== MEGAMENU VERTICAL ================================== -->
                                </li><!-- /.menu-item -->

                                <li class="dropdown menu-item">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon fa fa-apple fa-fw"></i>DJ Equipment</a>
                                    <ul class="dropdown-menu mega-menu">
                                        <li class="yamm-content">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Cartridges</a></li>
                                                        <li><a href="index.php?page=category">CD Players</a></li>
                                                        <li><a href="index.php?page=category">DJ Accessories</a></li>
                                                        <li><a href="index.php?page=category">DJ Controllers</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">DJ Headphones</a></li>
                                                        <li><a href="index.php?page=category">DJ Mixers</a></li>
                                                        <li><a href="index.php?page=category">DJ Packages</a></li>
                                                        <li><a href="index.php?page=category">DJ Video Projectors</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Headshells</a></li>
                                                        <li><a href="index.php?page=category">Interface</a></li>
                                                        <li><a href="index.php?page=category">Needles</a></li>
                                                        <li><a href="index.php?page=category">PA Combo Pack Deals</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Sample Pad</a></li>
                                                        <li><a href="index.php?page=category">Serato DJ Controller
                                                                Series</a></li>
                                                        <li><a href="index.php?page=category">Turntables</a></li>
                                                        <li><a href="index.php?page=category">Show All DJ Equipment</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.col -->
                                            </div><!-- /.row -->
                                        </li><!-- /.yamm-content -->
                                    </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->

                                <li class="dropdown menu-item">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon fa fa-camera fa-fw"></i>Musical Equipment</a>
                                    <ul class="dropdown-menu mega-menu">
                                        <li class="yamm-content">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Accessories</a></li>
                                                        <li><a href="index.php?page=category">Acoustic Guitar</a></li>
                                                        <li><a href="index.php?page=category">Amplifiers</a></li>
                                                        <li><a href="index.php?page=category">Bass Combo</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Bass Guitar</a></li>
                                                        <li><a href="index.php?page=category">Brass & Wind
                                                                Instruments</a></li>
                                                        <li><a href="index.php?page=category">Celo</a></li>
                                                        <li><a href="index.php?page=category">Conga</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Cymbals</a></li>
                                                        <li><a href="index.php?page=category">Drum Accessories</a></li>
                                                        <li><a href="index.php?page=category">Drums</a></li>
                                                        <li><a href="index.php?page=category">Effects Pedal</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Electric Drums</a></li>
                                                        <li><a href="index.php?page=category">Keyboard Amp</a></li>
                                                        <li><a href="index.php?page=category">Keyboards & Pianos</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">LEAD COMBO</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Lead Guitar</a></li>
                                                        <li><a href="index.php?page=category">Percussions</a></li>
                                                        <li><a href="index.php?page=category">Skins</a></li>
                                                        <li><a href="index.php?page=category">String Instruments</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Synthesizer</a></li>
                                                        <li><a href="index.php?page=category">Workstation</a></li>
                                                        <li><a href="index.php?page=category">Show All Musical
                                                                Equipment</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                            </div><!-- /.row -->
                                        </li><!-- /.yamm-content -->
                                    </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->

                                <li class="dropdown menu-item">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon fa fa-headphones fa-fw"></i>Microphones</a>
                                    <ul class="dropdown-menu mega-menu">
                                        <li class="yamm-content">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Combo Microphones</a></li>
                                                        <li><a href="index.php?page=category">Condenser Microphone</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Corded Microphones</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Drum Microphone</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Handheld Cordless
                                                                Microphones</a></li>
                                                        <li><a href="index.php?page=category">Handheld/Headset</a></li>
                                                        <li><a href="index.php?page=category">Handheld/Lapel</a></li>
                                                        <li><a href="index.php?page=category">Headsets</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Instrument Microphone</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Lapel Microphones</a></li>
                                                        <li><a href="index.php?page=category">Microphone Accessories</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Receivers</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Shotgun Microphone</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Video Microphone</a></li>
                                                        <li><a href="index.php?page=category">Wireless Camera
                                                                Microphone</a></li>
                                                        <li><a href="index.php?page=category">Wireless In-Ear
                                                                Monitors</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Wireless Microphone</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Show All Microphones</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.col -->
                                            </div><!-- /.row -->
                                        </li><!-- /.yamm-content -->
                                    </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->

                                <li class="dropdown menu-item">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon fa fa-gamepad fa-fw"></i>Professional Lighting</a>
                                    <ul class="dropdown-menu mega-menu">
                                        <li class="yamm-content">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Controllers</a></li>
                                                        <li><a href="index.php?page=category">Dimmer Packs</a></li>
                                                        <li><a href="index.php?page=category">Effect Lights</a></li>
                                                        <li><a href="index.php?page=category">Flame Machine</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Fluid & Liquid</a></li>
                                                        <li><a href="index.php?page=category">Foam Machine</a></li>
                                                        <li><a href="index.php?page=category">Fog & Bubbles</a></li>
                                                        <li><a href="index.php?page=category">Follow Spots</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Haze Machine</a></li>
                                                        <li><a href="index.php?page=category">Lasers</a></li>
                                                        <li><a href="index.php?page=category">LED Lights</a></li>
                                                        <li><a href="index.php?page=category">Light Controller</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Light Packages</a></li>
                                                        <li><a href="index.php?page=category">Mirror Balls</a></li>
                                                        <li><a href="index.php?page=category">Moving Heads</a></li>
                                                        <li><a href="index.php?page=category">Par Cans</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Scanners</a></li>
                                                        <li><a href="index.php?page=category">Smoke Machine</a></li>
                                                        <li><a href="index.php?page=category">Smoke Machine</a></li>
                                                        <li><a href="index.php?page=category">Software</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Stage Washes</a></li>
                                                        <li><a href="index.php?page=category">Stands & Acc</a></li>
                                                        <li><a href="index.php?page=category">Strobe Lights</a></li>
                                                        <li><a href="index.php?page=category">UV Lights</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Show All Professional
                                                                Lighting</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                            </div><!-- /.row -->
                                        </li><!-- /.yamm-content -->
                                    </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->

                                <li class="dropdown menu-item">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                            class="icon fa fa-location-arrow fa-fw"></i>Other</a>
                                    <ul class="dropdown-menu mega-menu">
                                        <li class="yamm-content">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Bags</a></li>
                                                        <li><a href="index.php?page=category">Bluetooth Headsets</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Cabling & Accessories</a>
                                                        </li>
                                                        <li><a href="index.php?page=category">Camera Microphones</a>
                                                        </li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Projectors</a></li>
                                                        <li><a href="index.php?page=category">Samplers</a></li>
                                                        <li><a href="index.php?page=category">Sound Module</a></li>
                                                        <li><a href="index.php?page=category">Stands</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                                <div class="col-sm-12 col-md-3">
                                                    <ul class="links list-unstyled">
                                                        <li><a href="index.php?page=category">Trussing & Stage</a></li>
                                                        <li><a href="index.php?page=category">Video Mixers</a></li>
                                                        <li><a href="index.php?page=category">Walkie Talkie</a></li>
                                                    </ul>
                                                </div><!-- /.col -->
                                            </div><!-- /.row -->
                                        </li><!-- /.yamm-content -->
                                    </ul><!-- /.dropdown-menu -->            </li><!-- /.menu-item -->


                            </ul><!-- /.nav -->
                        </nav><!-- /.megamenu-horizontal -->
                    </div><!-- /.side-menu -->
                    <!-- ================================== TOP NAVIGATION : END ================================== -->
                    <!-- ============================================== SPECIAL OFFER ============================================== -->

                    <div class="sidebar-widget outer-bottom-small wow fadeInUp">
                        <h3 class="section-title">Special Offer</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <div
                                class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
                                <div class="item">
                                    <div class="products special-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 130%"
                                                                        data-echo="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                        src="images/products/amplifiers/CPD3600-200x200.jpg" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->
                                                            <div class="tag tag-micro hot">
                                                                <span>hot</span>
                                                            </div>


                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">Amplifiers</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
                                                            <span class="price">
                                                                R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/drums/NITRO-200x200.png"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 130%"
                                                                        data-echo="images/products/drums/NITRO-200x200.png"
                                                                        src="images/products/drums/NITRO-200x200.png" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->


                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">Drums</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 130%"
                                                                        data-echo="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                        src="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->

                                                            <div class="tag tag-micro new">
                                                                <span>new</span>
                                                            </div>

                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">Headpones</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="products special-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 130%"
                                                                         data-echo="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                         src="images/products/amplifiers/CPD3600-200x200.jpg" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->
                                                            <div class="tag tag-micro hot">
                                                                <span>hot</span>
                                                            </div>


                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">Amplifiers</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
                                                            <span class="price">
                                                                R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/drums/NITRO-200x200.png"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 130%"
                                                                         data-echo="images/products/drums/NITRO-200x200.png"
                                                                         src="images/products/drums/NITRO-200x200.png" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->


                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">Drums</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 130%"
                                                                         data-echo="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                         src="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->

                                                            <div class="tag tag-micro new">
                                                                <span>new</span>
                                                            </div>

                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">Headpones</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="products special-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 130%"
                                                                         data-echo="images/products/amplifiers/CPD3600-200x200.jpg"
                                                                         src="images/products/amplifiers/CPD3600-200x200.jpg" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->
                                                            <div class="tag tag-micro hot">
                                                                <span>hot</span>
                                                            </div>


                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">Amplifiers</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
                                                            <span class="price">
                                                                R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/drums/NITRO-200x200.png"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 130%"
                                                                         data-echo="images/products/drums/NITRO-200x200.png"
                                                                         src="images/products/drums/NITRO-200x200.png" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->


                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">Drums</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 130%"
                                                                         data-echo="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg"
                                                                         src="images/products/headphones/AKG-K52-HEADPHONE-200x200.jpg" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->

                                                            <div class="tag tag-micro new">
                                                                <span>new</span>
                                                            </div>

                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">Headpones</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.sidebar-widget-body -->
                    </div><!-- /.sidebar-widget -->
                    <!-- ============================================== SPECIAL OFFER : END ============================================== -->
                    <!-- ============================================== PRODUCT TAGS ============================================== -->
                    <div class="sidebar-widget product-tag wow fadeInUp">
                        <h3 class="section-title">Product tags</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <div class="tag-list">
                                <a class="item" title="Phone" href="index.php?page=category">Sound</a>
                                <a class="item active" title="Vest" href="index.php?page=category">Studio</a>
                                <a class="item" title="Smartphone" href="index.php?page=category">DJ</a>
                                <a class="item" title="Furniture" href="index.php?page=category">Musical</a>
                                <a class="item" title="T-shirt" href="index.php?page=category">Microphones</a>
                                <a class="item" title="Sweatpants" href="index.php?page=category">Lighting</a>
                                <a class="item" title="Sneaker" href="index.php?page=category">Other</a>
                            </div><!-- /.tag-list -->
                        </div><!-- /.sidebar-widget-body -->
                    </div><!-- /.sidebar-widget -->
                    <!-- ============================================== PRODUCT TAGS : END ============================================== -->
                    <!-- ============================================== SPECIAL DEALS ============================================== -->

                    <!-- /.sidebar-widget -->
                    <!-- ============================================== SPECIAL DEALS : END ============================================== -->
                    <!-- ============================================== NEWSLETTER ============================================== -->
                    <div class="sidebar-widget newsletter wow fadeInUp outer-bottom-small">
                        <h3 class="section-title">Newsletters</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <p>Sign Up for Our Newsletter!</p>
                            <form role="form">
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1"
                                           placeholder="Subscribe to our newsletter">
                                </div>
                                <button class="btn btn-primary">Subscribe</button>
                            </form>
                        </div><!-- /.sidebar-widget-body -->
                    </div><!-- /.sidebar-widget -->
                    <!-- ============================================== NEWSLETTER: END ============================================== -->
                    <!-- ============================================== HOT DEALS ============================================== -->
                    <div class="sidebar-widget hot-deals wow fadeInUp">
                        <h3 class="section-title">hot deals</h3>
                        <div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-xs">

                            <div class="item">
                                <div class="products">
                                    <div class="hot-deal-wrapper">
                                        <div class="image">
                                            <img src="images/products/speakers/HYBRID+ HP218_(Front)_web-228x228.png" alt="">
                                        </div>
                                        <div class="sale-offer-tag"><span>35%<br>off</span></div>
                                        <div class="timing-wrapper">
                                            <div class="box-wrapper">
                                                <div class="date box">
                                                    <span class="key">120</span>
                                                    <span class="value">Days</span>
                                                </div>
                                            </div>

                                            <div class="box-wrapper">
                                                <div class="hour box">
                                                    <span class="key">20</span>
                                                    <span class="value">HRS</span>
                                                </div>
                                            </div>

                                            <div class="box-wrapper">
                                                <div class="minutes box">
                                                    <span class="key">36</span>
                                                    <span class="value">MINS</span>
                                                </div>
                                            </div>

                                            <div class="box-wrapper hidden-md">
                                                <div class="seconds box">
                                                    <span class="key">60</span>
                                                    <span class="value">SEC</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.hot-deal-wrapper -->

                                    <div class="product-info text-left m-t-20">
                                        <h3 class="name"><a href="index.php?page=detail">HYBRID + HP218 DUAL 18" SUB 2200 WATT</a>
                                        </h3>
                                        <div class="rating rateit-small"></div>

                                        <div class="product-price">
								<span class="price">
									R600.00
								</span>

                                            <span class="price-before-discount">R800.00</span>

                                        </div><!-- /.product-price -->

                                    </div><!-- /.product-info -->

                                    <div class="cart clearfix animate-effect">
                                        <div class="action">

                                            <div class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown"
                                                        type="button">
                                                    <i class="fa fa-shopping-cart"></i>
                                                </button>
                                                <button class="btn btn-primary" type="button">Add to cart</button>

                                            </div>

                                        </div><!-- /.action -->
                                    </div><!-- /.cart -->
                                </div>
                            </div>
                            <div class="item">
                                <div class="products">
                                    <div class="hot-deal-wrapper">
                                        <div class="image">
                                            <img src="images/products/other/apc40_web_large_700x438-228x228.png" alt="">
                                        </div>
                                        <div class="sale-offer-tag"><span>35%<br>off</span></div>
                                        <div class="timing-wrapper">
                                            <div class="box-wrapper">
                                                <div class="date box">
                                                    <span class="key">120</span>
                                                    <span class="value">Days</span>
                                                </div>
                                            </div>

                                            <div class="box-wrapper">
                                                <div class="hour box">
                                                    <span class="key">20</span>
                                                    <span class="value">HRS</span>
                                                </div>
                                            </div>

                                            <div class="box-wrapper">
                                                <div class="minutes box">
                                                    <span class="key">36</span>
                                                    <span class="value">MINS</span>
                                                </div>
                                            </div>

                                            <div class="box-wrapper hidden-md">
                                                <div class="seconds box">
                                                    <span class="key">60</span>
                                                    <span class="value">SEC</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.hot-deal-wrapper -->

                                    <div class="product-info text-left m-t-20">
                                        <h3 class="name"><a href="index.php?page=detail">Akai APC40 Ableton Performance Controller</a>
                                        </h3>
                                        <div class="rating rateit-small"></div>

                                        <div class="product-price">
								<span class="price">
									R600.00
								</span>

                                            <span class="price-before-discount">R800.00</span>

                                        </div><!-- /.product-price -->

                                    </div><!-- /.product-info -->

                                    <div class="cart clearfix animate-effect">
                                        <div class="action">

                                            <div class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown"
                                                        type="button">
                                                    <i class="fa fa-shopping-cart"></i>
                                                </button>
                                                <button class="btn btn-primary" type="button">Add to cart</button>

                                            </div>

                                        </div><!-- /.action -->
                                    </div><!-- /.cart -->
                                </div>
                            </div>
                            <div class="item">
                                <div class="products">
                                    <div class="hot-deal-wrapper">
                                        <div class="image">
                                            <img src="images/products/other/wk240_1bigger-228x228.jpg" alt="">
                                        </div>
                                        <div class="sale-offer-tag"><span>35%<br>off</span></div>
                                        <div class="timing-wrapper">
                                            <div class="box-wrapper">
                                                <div class="date box">
                                                    <span class="key">120</span>
                                                    <span class="value">Days</span>
                                                </div>
                                            </div>

                                            <div class="box-wrapper">
                                                <div class="hour box">
                                                    <span class="key">20</span>
                                                    <span class="value">HRS</span>
                                                </div>
                                            </div>

                                            <div class="box-wrapper">
                                                <div class="minutes box">
                                                    <span class="key">36</span>
                                                    <span class="value">MINS</span>
                                                </div>
                                            </div>

                                            <div class="box-wrapper hidden-md">
                                                <div class="seconds box">
                                                    <span class="key">60</span>
                                                    <span class="value">SEC</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.hot-deal-wrapper -->

                                    <div class="product-info text-left m-t-20">
                                        <h3 class="name"><a href="index.php?page=detail">RODE NTG3B SHOTGUN CONDENSOR MICROPHONE</a>
                                        </h3>
                                        <div class="rating rateit-small"></div>

                                        <div class="product-price">
								<span class="price">
									R600.00
								</span>

                                            <span class="price-before-discount">R800.00</span>

                                        </div><!-- /.product-price -->

                                    </div><!-- /.product-info -->

                                    <div class="cart clearfix animate-effect">
                                        <div class="action">

                                            <div class="add-cart-button btn-group">
                                                <button class="btn btn-primary icon" data-toggle="dropdown"
                                                        type="button">
                                                    <i class="fa fa-shopping-cart"></i>
                                                </button>
                                                <button class="btn btn-primary" type="button">Add to cart</button>

                                            </div>

                                        </div><!-- /.action -->
                                    </div><!-- /.cart -->
                                </div>
                            </div>


                        </div><!-- /.sidebar-widget -->
                    </div>
                    <!-- ============================================== HOT DEALS: END ============================================== -->
                    <!-- ============================================== COLOR============================================== -->
{{--                    <div class="sidebar-widget  wow fadeInUp outer-top-vs ">--}}
{{--                        <div id="advertisement" class="advertisement">--}}
{{--                            <div class="item bg-color">--}}
{{--                                <div class="container-fluid">--}}
{{--                                    <div class="caption vertical-top text-left">--}}
{{--                                        <div class="big-text">--}}
{{--                                            Save<span class="big">50%</span>--}}
{{--                                        </div>--}}


{{--                                        <div class="excerpt">--}}
{{--                                            on selected items--}}
{{--                                        </div>--}}
{{--                                    </div><!-- /.caption -->--}}
{{--                                </div><!-- /.container-fluid -->--}}
{{--                            </div><!-- /.item -->--}}

{{--                            <div class="item" style="background-image: url('theme/assets/images/advertisement/1.jpg');">--}}

{{--                            </div><!-- /.item -->--}}

{{--                            <div class="item bg-color">--}}
{{--                                <div class="container-fluid">--}}
{{--                                    <div class="caption vertical-top text-left">--}}
{{--                                        <div class="big-text">--}}
{{--                                            Save<span class="big">50%</span>--}}
{{--                                        </div>--}}


{{--                                        <div class="excerpt fadeInDown-2">--}}
{{--                                            on selected items--}}
{{--                                        </div>--}}
{{--                                    </div><!-- /.caption -->--}}
{{--                                </div><!-- /.container-fluid -->--}}
{{--                            </div><!-- /.item -->--}}

{{--                        </div><!-- /.owl-carousel -->--}}
{{--                    </div>--}}

                    <!-- ============================================== COLOR: END ============================================== -->


                </div><!-- /.sidemenu-holder -->
                <!-- ============================================== SIDEBAR : END ============================================== -->

                <!-- ============================================== CONTENT ============================================== -->
                <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
                    <!-- ========================================== SECTION – HERO ========================================= -->

                    <div id="hero">
                        <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">

                            <div class="item offer1"
                                 style="background-image: url(images/ads/presonus_eris_e3.5_studio_monitors_1024x1024.jpg);">
                                <div class="container-fluid">
                                    <div class="caption bg-color vertical-center text-left">
                                        <div class="big-text fadeInDown-1">
                                            The new <span class="highlight">PreSonus</span> for you
                                        </div>

                                        <div class="excerpt fadeInDown-2 hidden-xs">

                                            <span>Eris E3.5 Now Starting At R2099 </span>
                                            <span>Eris E4.5 Starting At R3799</span>
                                        </div>
                                        <div class="button-holder fadeInDown-3">
                                            <a href="index.php?page=single-product"
                                               class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a>
                                        </div>
                                    </div><!-- /.caption -->
                                </div><!-- /.container-fluid -->
                            </div><!-- /.item -->

                            <div class="item offer2" style="background-image: url(images/ads/cdj-2000nexus-main.png);">
                                <div class="container-fluid">
                                    <div class="caption bg-color vertical-center text-left">
                                        <div class="big-text fadeInDown-1">
                                            The new <span class="highlight">PIONEER CDJ</span> for you
                                        </div>

                                        <div class="excerpt fadeInDown-2 hidden-xs">

                                            <span>CDJ-2000NXS Starting At R1799</span>
                                            <span>PURE PERFORMANCE.</span>
                                        </div>
                                        <div class="button-holder fadeInDown-3">
                                            <a href="index.php?page=single-product"
                                               class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a>
                                        </div>
                                    </div><!-- /.caption -->
                                </div><!-- /.container-fluid -->
                            </div><!-- /.item -->


                        </div><!-- /.owl-carousel -->
                    </div>

                    <!-- ========================================= SECTION – HERO : END ========================================= -->

                    <!-- ============================================== INFO BOXES ============================================== -->
                    <div class="info-boxes wow fadeInUp">
                        <div class="info-boxes-inner">
                            <div class="row">
                                <div class="col-md-6 col-sm-4 col-lg-4">
                                    <div class="info-box">
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <i class="icon fa fa-money"></i>
                                            </div>
                                            <div class="col-xs-10">
                                                <h4 class="info-box-heading green">money back</h4>
                                            </div>
                                        </div>
                                        <h6 class="text">30 Day Money Back Guarantee.</h6>
                                    </div>
                                </div><!-- .col -->

                                <div class="hidden-md col-sm-4 col-lg-4">
                                    <div class="info-box">
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <i class="icon fa fa-truck"></i>
                                            </div>
                                            <div class="col-xs-10">
                                                <h4 class="info-box-heading orange">free shipping</h4>
                                            </div>
                                        </div>
                                        <h6 class="text">free ship-on oder over R600.00</h6>
                                    </div>
                                </div><!-- .col -->

                                <div class="col-md-6 col-sm-4 col-lg-4">
                                    <div class="info-box">
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <i class="icon fa fa-gift"></i>
                                            </div>
                                            <div class="col-xs-10">
                                                <h4 class="info-box-heading red">Special Sale</h4>
                                            </div>
                                        </div>
                                        <h6 class="text">All items-sale up to 20% off </h6>
                                    </div>
                                </div><!-- .col -->
                            </div><!-- /.row -->
                        </div><!-- /.info-boxes-inner -->

                    </div><!-- /.info-boxes -->
                    <!-- ============================================== INFO BOXES : END ============================================== -->
                    <!-- ============================================== SCROLL TABS ============================================== -->
                    <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
                        <div class="more-info-tab clearfix ">
                            <h3 class="new-product-title pull-left">New Products</h3>
                            <ul class="nav nav-tabs nav-tab-line pull-right" id="new-products-1">
                                <li class="active"><a data-transition-type="backSlide" href="#all"
                                                      data-toggle="tab">All</a></li>
                                <li><a data-transition-type="backSlide" href="#smartphone"data-toggle="tab">MIXERS</a></li>
                                <li><a data-transition-type="backSlide" href="#laptop" data-toggle="tab">Headphones</a></li>
                                <li><a data-transition-type="backSlide" href="#apple" data-toggle="tab">Studio Monitors</a></li>
                            </ul><!-- /.nav-tabs -->
                        </div>

                        <div class="tab-content outer-top-xs">
                            <div class="tab-pane in active" id="all">
                                <div class="product-slider">
                                    <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img style="width: 70%"
                                                                    src="images/products/mixers/ALTO LIVE-1604 ANALOG MIXER-200x200.jpg"
                                                                    data-echo="images/products/mixers/ALTO LIVE-1604 ANALOG MIXER-200x200.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag sale"><span>sale</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">ALTO LIVE-1604 ANALOG MIXER</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img style="width: 70%"
                                                                    src="images/products/microphones/LR 616-200x200.jpeg"
                                                                    data-echo="images/products/microphones/LR 616-200x200.jpeg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag hot"><span>hot</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">LANE LR-616 DUAL HANDHELD WIRELESS MIC SET</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img style="width: 70%"
                                                                    src="images/products/keyboards/yamaha-psr-e463-200x200.jpg"
                                                                    data-echo="images/products/keyboards/yamaha-psr-e463-200x200.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag new"><span>new</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Yamaha PSR-E463 61-key Portable Keyboard</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img style="width: 70%"
                                                                     src="images/products/drums/Yamaha_DD65-200x200.jpg"
                                                                     data-echo="images/products/drums/Yamaha_DD65-200x200.jpg"
                                                                     alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag hot"><span>hot</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Yamaha DD-65 Digital Drums</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img style="width: 70%"
                                                                     src="images/products/studio-monitors/KRK8-200x200.jpg"
                                                                     data-echo="images/products/studio-monitors/KRK8-200x200.jpg"
                                                                     alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag sale"><span>sale</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">KRK ROKIT 8" POWERD STUDIO MONITOR PAIR</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img style="width: 70%"
                                                                     src="images/products/amplifiers/ep4000-200x200.jpg"
                                                                     data-echo="images/products/amplifiers/ep4000-200x200.jpg"
                                                                     alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag new"><span>new</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">BEHRINGER EP4000 POWER AMPLIFIER</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->
                                    </div><!-- /.home-owl-carousel -->
                                </div><!-- /.product-slider -->
                            </div><!-- /.tab-pane -->

                            <div class="tab-pane" id="smartphone">
                                <div class="product-slider">
                                    <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/2.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag new"><span>new</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy
                                                                S4</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/3.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag sale"><span>sale</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Apple Iphone 5s
                                                                32GB</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/6.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag new"><span>new</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Nokia Lumia
                                                                520</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/2.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag hot"><span>hot</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy
                                                                S4</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/4.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag sale"><span>sale</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">LG Smart Phone
                                                                LP68</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/1.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag hot"><span>hot</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Sony Ericson
                                                                Vaga</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->
                                    </div><!-- /.home-owl-carousel -->
                                </div><!-- /.product-slider -->
                            </div><!-- /.tab-pane -->

                            <div class="tab-pane" id="laptop">
                                <div class="product-slider">
                                    <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/4.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag sale"><span>sale</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">LG Smart Phone
                                                                LP68</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/3.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag sale"><span>sale</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Apple Iphone 5s
                                                                32GB</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/1.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag hot"><span>hot</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Sony Ericson
                                                                Vaga</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/2.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag new"><span>new</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy
                                                                S4</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/2.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag hot"><span>hot</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy
                                                                S4</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/6.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag new"><span>new</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Nokia Lumia
                                                                520</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->
                                    </div><!-- /.home-owl-carousel -->
                                </div><!-- /.product-slider -->
                            </div><!-- /.tab-pane -->

                            <div class="tab-pane" id="apple">
                                <div class="product-slider">
                                    <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/4.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag sale"><span>sale</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">LG Smart Phone
                                                                LP68</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/6.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag new"><span>new</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Nokia Lumia
                                                                520</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/2.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag hot"><span>hot</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy
                                                                S4</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/2.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag new"><span>new</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Samsung Galaxy
                                                                S4</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/1.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag hot"><span>hot</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Sony Ericson
                                                                Vaga</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="index.php?page=detail"><img
                                                                    src="theme/assets/images/blank.gif"
                                                                    data-echo="theme/assets/images/products/3.jpg"
                                                                    alt=""></a>
                                                        </div><!-- /.image -->

                                                        <div class="tag sale"><span>sale</span></div>
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="index.php?page=detail">Apple Iphone 5s
                                                                32GB</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                            <span class="price-before-discount">R 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon"
                                                                            data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary" type="button">Add to
                                                                        cart
                                                                    </button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="index.php?page=detail"
                                                                       title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->
                                    </div><!-- /.home-owl-carousel -->
                                </div><!-- /.product-slider -->
                            </div><!-- /.tab-pane -->

                        </div><!-- /.tab-content -->
                    </div><!-- /.scroll-tabs -->
                    <!-- ============================================== SCROLL TABS : END ============================================== -->
                    <!-- ============================================== WIDE PRODUCTS ============================================== -->
                    <div class="wide-banners wow fadeInUp outer-bottom-vs">
                        <div class="row">

                            <div class="col-md-7">
                                <div class="wide-banner cnt-strip">
                                    <div class="image">
                                        <img class="img-responsive" data-echo="theme/assets/images/banners/1.jpg"
                                             src="theme/assets/images/blank.gif" alt="">
                                    </div>
                                    <div class="strip">
                                        <div class="strip-inner">
                                            <h3 class="hidden-xs">samsung</h3>
                                            <h2>galaxy</h2>
                                        </div>
                                    </div>
                                </div><!-- /.wide-banner -->
                            </div><!-- /.col -->

                            <div class="col-md-5">
                                <div class="wide-banner cnt-strip">
                                    <div class="image">
                                        <img class="img-responsive" data-echo="theme/assets/images/banners/2.jpg"
                                             src="theme/assets/images/blank.gif" alt="">
                                    </div>
                                    <div class="strip">
                                        <div class="strip-inner">
                                            <h3 class="hidden-xs">new trend</h3>
                                            <h2>watch phone</h2>
                                        </div>
                                    </div>
                                </div><!-- /.wide-banner -->
                            </div><!-- /.col -->

                        </div><!-- /.row -->
                    </div><!-- /.wide-banners -->

                    <!-- ============================================== WIDE PRODUCTS : END ============================================== -->
                    <!-- ============================================== FEATURED PRODUCTS ============================================== -->
                    <section class="section featured-product wow fadeInUp">
                        <h3 class="section-title">Featured products</h3>
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">

                            <div class="item item-carousel">
                                <div class="products">

                                    <div class="product">
                                        <div class="product-image">
                                            <div class="image">
                                                <a href="index.php?page=detail"><img style="width: 70%"
                                                    src="images/products/other/10063217_hfc_v1-228x228.jpg"
                                                    data-echo="images/products/other/10063217_hfc_v1-228x228.jpg"
                                                     alt=""></a>
                                            </div><!-- /.image -->

                                            <div class="tag sale"><span>sale</span></div>
                                        </div><!-- /.product-image -->


                                        <div class="product-info text-left">
                                            <h3 class="name"><a href="index.php?page=detail">FOTOMATE PROJECTOR FM290PL</a>
                                            </h3>
                                            <div class="rating rateit-small"></div>
                                            <div class="description"></div>

                                            <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                <span class="price-before-discount">R 800</span>

                                            </div><!-- /.product-price -->

                                        </div><!-- /.product-info -->
                                        <div class="cart clearfix animate-effect">
                                            <div class="action">
                                                <ul class="list-unstyled">
                                                    <li class="add-cart-button btn-group">
                                                        <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                type="button">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </button>
                                                        <button class="btn btn-primary" type="button">Add to cart
                                                        </button>

                                                    </li>

                                                    <li class="lnk wishlist">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Wishlist">
                                                            <i class="icon fa fa-heart"></i>
                                                        </a>
                                                    </li>

                                                    <li class="lnk">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Compare">
                                                            <i class="fa fa-retweet"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div><!-- /.action -->
                                        </div><!-- /.cart -->
                                    </div><!-- /.product -->

                                </div><!-- /.products -->
                            </div><!-- /.item -->

                            <div class="item item-carousel">
                                <div class="products">

                                    <div class="product">
                                        <div class="product-image">
                                            <div class="image">
                                                <a href="index.php?page=detail"><img style="width: 70%"
                                                                                     src="images/products/other/AKGK181_Medium-228x228.jpg"
                                                                                     data-echo="images/products/other/AKGK181_Medium-228x228.jpg"
                                                                                     alt=""></a>
                                            </div><!-- /.image -->

                                            <div class="tag hot"><span>hot</span></div>
                                        </div><!-- /.product-image -->


                                        <div class="product-info text-left">
                                            <h3 class="name"><a href="index.php?page=detail">AKG K181 DJ HEADPHONES</a></h3>
                                            <div class="rating rateit-small"></div>
                                            <div class="description"></div>

                                            <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                <span class="price-before-discount">R 800</span>

                                            </div><!-- /.product-price -->

                                        </div><!-- /.product-info -->
                                        <div class="cart clearfix animate-effect">
                                            <div class="action">
                                                <ul class="list-unstyled">
                                                    <li class="add-cart-button btn-group">
                                                        <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                type="button">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </button>
                                                        <button class="btn btn-primary" type="button">Add to cart
                                                        </button>

                                                    </li>

                                                    <li class="lnk wishlist">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Wishlist">
                                                            <i class="icon fa fa-heart"></i>
                                                        </a>
                                                    </li>

                                                    <li class="lnk">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Compare">
                                                            <i class="fa fa-retweet"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div><!-- /.action -->
                                        </div><!-- /.cart -->
                                    </div><!-- /.product -->

                                </div><!-- /.products -->
                            </div><!-- /.item -->

                            <div class="item item-carousel">
                                <div class="products">

                                    <div class="product">
                                        <div class="product-image">
                                            <div class="image">
                                                <a href="index.php?page=detail"><img style="width: 70%"
                                                                                     src="images/products/other/ALGATE-228x228.jpg"
                                                                                     data-echo="images/products/other/ALGATE-228x228.jpg"
                                                                                     alt=""></a>
                                            </div><!-- /.image -->

                                            <div class="tag new"><span>new</span></div>
                                        </div><!-- /.product-image -->


                                        <div class="product-info text-left">
                                            <h3 class="name"><a href="index.php?page=detail">ALUSTAGE DECOR BLACK GOAL POST</a></h3>
                                            <div class="rating rateit-small"></div>
                                            <div class="description"></div>

                                            <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                <span class="price-before-discount">R 800</span>

                                            </div><!-- /.product-price -->

                                        </div><!-- /.product-info -->
                                        <div class="cart clearfix animate-effect">
                                            <div class="action">
                                                <ul class="list-unstyled">
                                                    <li class="add-cart-button btn-group">
                                                        <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                type="button">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </button>
                                                        <button class="btn btn-primary" type="button">Add to cart
                                                        </button>

                                                    </li>

                                                    <li class="lnk wishlist">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Wishlist">
                                                            <i class="icon fa fa-heart"></i>
                                                        </a>
                                                    </li>

                                                    <li class="lnk">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Compare">
                                                            <i class="fa fa-retweet"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div><!-- /.action -->
                                        </div><!-- /.cart -->
                                    </div><!-- /.product -->

                                </div><!-- /.products -->
                            </div><!-- /.item -->

                            <div class="item item-carousel">
                                <div class="products">

                                    <div class="product">
                                        <div class="product-image">
                                            <div class="image">
                                                <a href="index.php?page=detail"><img style="width: 70%"
                                                                                     src="images/products/other/CRSK1045-228x228.jpg"
                                                                                     data-echo="images/products/other/CRSK1045-228x228.jpg"
                                                                                     alt=""></a>
                                            </div><!-- /.image -->

                                            <div class="tag new"><span>new</span></div>
                                        </div><!-- /.product-image -->


                                        <div class="product-info text-left">
                                            <h3 class="name"><a href="index.php?page=detail">CROSSROCK CRSK1045</a></h3>
                                            <div class="rating rateit-small"></div>
                                            <div class="description"></div>

                                            <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                <span class="price-before-discount">R 800</span>

                                            </div><!-- /.product-price -->

                                        </div><!-- /.product-info -->
                                        <div class="cart clearfix animate-effect">
                                            <div class="action">
                                                <ul class="list-unstyled">
                                                    <li class="add-cart-button btn-group">
                                                        <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                type="button">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </button>
                                                        <button class="btn btn-primary" type="button">Add to cart
                                                        </button>

                                                    </li>

                                                    <li class="lnk wishlist">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Wishlist">
                                                            <i class="icon fa fa-heart"></i>
                                                        </a>
                                                    </li>

                                                    <li class="lnk">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Compare">
                                                            <i class="fa fa-retweet"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div><!-- /.action -->
                                        </div><!-- /.cart -->
                                    </div><!-- /.product -->

                                </div><!-- /.products -->
                            </div><!-- /.item -->

                            <div class="item item-carousel">
                                <div class="products">

                                    <div class="product">
                                        <div class="product-image">
                                            <div class="image">
                                                <a href="index.php?page=detail"><img style="width: 70%"
                                                                                     src="images/products/other/E45BT-228x228.jpg"
                                                                                     data-echo="images/products/other/E45BT-228x228.jpg"
                                                                                     alt=""></a>
                                            </div><!-- /.image -->

                                            <div class="tag hot"><span>hot</span></div>
                                        </div><!-- /.product-image -->


                                        <div class="product-info text-left">
                                            <h3 class="name"><a href="index.php?page=detail">JBL E45BT WIRELESS ON EAR HEADPHONE</a></h3>
                                            <div class="rating rateit-small"></div>
                                            <div class="description"></div>

                                            <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                <span class="price-before-discount">R 800</span>

                                            </div><!-- /.product-price -->

                                        </div><!-- /.product-info -->
                                        <div class="cart clearfix animate-effect">
                                            <div class="action">
                                                <ul class="list-unstyled">
                                                    <li class="add-cart-button btn-group">
                                                        <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                type="button">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </button>
                                                        <button class="btn btn-primary" type="button">Add to cart
                                                        </button>

                                                    </li>

                                                    <li class="lnk wishlist">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Wishlist">
                                                            <i class="icon fa fa-heart"></i>
                                                        </a>
                                                    </li>

                                                    <li class="lnk">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Compare">
                                                            <i class="fa fa-retweet"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div><!-- /.action -->
                                        </div><!-- /.cart -->
                                    </div><!-- /.product -->

                                </div><!-- /.products -->
                            </div><!-- /.item -->

                            <div class="item item-carousel">
                                <div class="products">

                                    <div class="product">
                                        <div class="product-image">
                                            <div class="image">
                                                <a href="index.php?page=detail"><img style="width: 70%"
                                                                                     src="images/products/other/integra-7_front_gal-228x228.jpg"
                                                                                     data-echo="images/products/other/integra-7_front_gal-228x228.jpg"
                                                                                     alt=""></a>
                                            </div><!-- /.image -->

                                            <div class="tag sale"><span>sale</span></div>
                                        </div><!-- /.product-image -->


                                        <div class="product-info text-left">
                                            <h3 class="name"><a href="index.php?page=detail">ROLAND INTEGRA-7 SOUND MODULE</a>
                                            </h3>
                                            <div class="rating rateit-small"></div>
                                            <div class="description"></div>

                                            <div class="product-price">
				<span class="price">
					R650.99				</span>
                                                <span class="price-before-discount">R 800</span>

                                            </div><!-- /.product-price -->

                                        </div><!-- /.product-info -->
                                        <div class="cart clearfix animate-effect">
                                            <div class="action">
                                                <ul class="list-unstyled">
                                                    <li class="add-cart-button btn-group">
                                                        <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                type="button">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </button>
                                                        <button class="btn btn-primary" type="button">Add to cart
                                                        </button>

                                                    </li>

                                                    <li class="lnk wishlist">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Wishlist">
                                                            <i class="icon fa fa-heart"></i>
                                                        </a>
                                                    </li>

                                                    <li class="lnk">
                                                        <a class="add-to-cart" href="index.php?page=detail"
                                                           title="Compare">
                                                            <i class="fa fa-retweet"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div><!-- /.action -->
                                        </div><!-- /.cart -->
                                    </div><!-- /.product -->

                                </div><!-- /.products -->
                            </div><!-- /.item -->
                        </div><!-- /.home-owl-carousel -->
                    </section><!-- /.section -->
                    <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->
                    <!-- ============================================== WIDE PRODUCTS ============================================== -->
                    <div class="wide-banners wow fadeInUp outer-bottom-vs">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="wide-banner cnt-strip">
                                    <div class="image">
                                        <img class="img-responsive" data-echo="theme/assets/images/banners/3.jpg"
                                             src="theme/assets/images/blank.gif" alt="">
                                    </div>
                                    <div class="strip strip-text">
                                        <div class="strip-inner">
                                            <h2 class="text-right">one stop place for<br>
                                                <span class="shopping-needs">all your shopping needs</span></h2>
                                        </div>
                                    </div>
                                    <div class="new-label">
                                        <div class="text">NEW</div>
                                    </div><!-- /.new-label -->
                                </div><!-- /.wide-banner -->
                            </div><!-- /.col -->

                        </div><!-- /.row -->
                    </div><!-- /.wide-banners -->
                    <!-- ============================================== WIDE PRODUCTS : END ============================================== -->
                    <!-- ============================================== BEST SELLER ============================================== -->

                    <div class="sidebar-widget wow fadeInUp outer-bottom-vs">
                        <h3 class="section-title">Best seller</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <div class="owl-carousel best-seller custom-carousel owl-theme outer-top-xs">
                                <div class="item">
                                    <div class="products best-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/other/WA004-228x228.jpg"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 100%"
                                                                         data-echo="images/products/other/WA004-228x228.jpg"
                                                                         src="images/products/other/WA004-228x228.jpg" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->

                                                            <div class="tag tag-micro new">
                                                                <span>new</span>
                                                            </div>

                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">WARM AUDIO WA-2A OPTICAL COMPRESSOR</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
                                                            <span class="price">
                                                                R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="products best-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/other/pioneer-ddj-starter-kit-228x228.jpg"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 100%"
                                                                         data-echo="images/products/other/pioneer-ddj-starter-kit-228x228.jpg"
                                                                         src="images/products/other/pioneer-ddj-starter-kit-228x228.jpg" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->

                                                            <div class="tag tag-micro new">
                                                                <span>new</span>
                                                            </div>

                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">PIONEER DJ STARTER PACK</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="products best-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/other/Pioneer-XDJ-1000-228x228.jpg"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 100%"
                                                                         data-echo="images/products/other/Pioneer-XDJ-1000-228x228.jpg"
                                                                         src="images/products/other/WPioneer-XDJ-1000-228x228.jpg" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->

                                                            <div class="tag tag-micro new">
                                                                <span>new</span>
                                                            </div>

                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">PIONEER XDJ1000 <br> </a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="products best-product">
                                        <div class="product">
                                            <div class="product-micro">
                                                <div class="row product-micro-row">
                                                    <div class="col col-xs-5">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <a href="images/products/other/PUSH-2-228x228.jpg"
                                                                   data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                    <img style="width: 100%"
                                                                         data-echo="images/products/other/PUSH-2-228x228.jpg"
                                                                         src="images/products/other/PUSH-2-228x228.jpg" alt="">
                                                                    <div class="zoom-overlay"></div>
                                                                </a>
                                                            </div><!-- /.image -->

                                                            <div class="tag tag-micro new">
                                                                <span>new</span>
                                                            </div>

                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-xs-7">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="#">ABLETON PUSH-2 SAMPLE PAD & DJ CONTROLLER</a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">
				<span class="price">
					R650.99				</span>

                                                            </div><!-- /.product-price -->
                                                            <div class="action"><a href="#" class="lnk btn btn-primary">Add
                                                                    To Cart</a></div>
                                                        </div>
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-micro-row -->
                                            </div><!-- /.product-micro -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.sidebar-widget-body -->
                    </div><!-- /.sidebar-widget -->
                    <!-- ============================================== BEST SELLER : END ============================================== -->

                    <!-- ============================================== BLOG SLIDER ============================================== -->
                    <section class="section outer-bottom-vs wow fadeInUp">
                        <h3 class="section-title">latest form blog</h3>
                        <div class="blog-slider-container outer-top-xs">
                            <div class="owl-carousel blog-slider custom-carousel">

                                <div class="item">
                                    <div class="blog-post">
                                        <div class="blog-post-image">
                                            <div class="image justify-content-center">
                                                <a href="index.php?page=blog"><img style="width: 50%; height: auto;"
                                                                                   data-echo="images/products/other/E45BT-228x228.jpg" width="270"
                                                                                   height="135" src="images/products/other/E45BT-228x228.jpg" alt=""></a>
                                            </div>
                                        </div><!-- /.blog-post-image -->


                                        <div class="blog-post-info text-left">
                                            <h3 class="name"><a href="#">JBL E45BT WIRELESS ON EAR HEADPHONE</a></h3>
                                            <a href="https://www.youtube.com/watch?v=4M9vd6BCxKw" target="_blank"  class="lnk btn btn-primary">Watch Video</a>
                                        </div><!-- /.blog-post-info -->


                                    </div><!-- /.blog-post -->
                                </div><!-- /.item -->


                                <div class="item">
                                    <div class="blog-post">
                                        <div class="blog-post-image">
                                            <div class="image">
                                                <a href="index.php?page=blog"><img style="width: 50%; height: auto;"
                                                                                   data-echo="images/products/other/MPX16_angle_web_700x438-228x228.png" width="270"
                                                                                   height="135" src="images/products/other/MPX16_angle_web_700x438-228x228.png" alt=""></a>
                                            </div>
                                        </div><!-- /.blog-post-image -->


                                        <div class="blog-post-info text-left">
                                            <h3 class="name"><a href="#">AKAI Professional MPX16 Sample Recorder</a></h3>
                                            <a href="https://www.youtube.com/watch?v=_VBP8m6KiTY" target="_blank" class="lnk btn btn-primary">Watch Video</a>
                                        </div><!-- /.blog-post-info -->


                                    </div><!-- /.blog-post -->
                                </div><!-- /.item -->


                                <div class="item">
                                    <div class="blog-post">
                                        <div class="blog-post-image">
                                            <div class="image">
                                                <a href="index.php?page=blog"><img style="width: 50%; height: auto;"
                                                                                   data-echo="images/products/other/denon-ddj-prime-4-front-thedjshop-228x228.jpg" width="270"
                                                                                   height="135" src="images/products/other/denon-ddj-prime-4-front-thedjshop-228x228.jpg" alt=""></a>
                                            </div>
                                        </div><!-- /.blog-post-image -->


                                        <div class="blog-post-info text-left">
                                            <h3 class="name"><a href="#">Denon DJ Prime 4 Feature Overview</a></h3>
                                            <a href="https://www.youtube.com/watch?v=i9Zz0o-0B8E" target="_blank" class="lnk btn btn-primary">Watch Video</a>
                                        </div><!-- /.blog-post-info -->


                                    </div><!-- /.blog-post -->
                                </div><!-- /.item -->


                                <div class="item">
                                    <div class="blog-post">
                                        <div class="blog-post-image">
                                            <div class="image">
                                                <a href="index.php?page=blog"><img style="width: 50%; height: auto"
                                                                                   data-echo="images/products/other/DEN-SM50-228x228.jpg" width="270"
                                                                                   height="135" src="images/products/other/DEN-SM50-228x228.jpg" alt=""></a>
                                            </div>
                                        </div><!-- /.blog-post-image -->


                                        <div class="blog-post-info text-left">
                                            <h3 class="name"><a href="#">DENON SM50 MONITOR (PAIR)</a></h3>
                                            <a href="https://www.youtube.com/watch?v=yqSaTMbJuRM" target="_blank" class="lnk btn btn-primary">Watch Video</a>
                                        </div><!-- /.blog-post-info -->


                                    </div><!-- /.blog-post -->
                                </div><!-- /.item -->


                                <div class="item">
                                    <div class="blog-post">
                                        <div class="blog-post-image">
                                            <div class="image">
                                                <a href="index.php?page=blog"><img style="width: 50%; height: auto"
                                                                                   data-echo="images/products/other/CCLL1-RDM-228x228.jpg" width="270"
                                                                                   height="135" src="images/products/other/CCLL1-RDM-228x228.jpg" alt=""></a>
                                            </div>
                                        </div><!-- /.blog-post-image -->


                                        <div class="blog-post-info text-left">
                                            <h3 class="name"><a href="#">COLE CARK LITTLE LADY 1 CCLL1-RDM</a></h3>
                                            <a href="https://www.youtube.com/watch?v=BaKRB49HeFU" target="_blank" class="lnk btn btn-primary">Watch Video</a>
                                        </div><!-- /.blog-post-info -->


                                    </div><!-- /.blog-post -->
                                </div><!-- /.item -->
                            </div><!-- /.owl-carousel -->
                        </div><!-- /.blog-slider-container -->
                    </section><!-- /.section -->
                    <!-- ============================================== BLOG SLIDER : END ============================================== -->

                    <!-- ============================================== FEATURED PRODUCTS ============================================== -->

                    <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->

                </div><!-- /.homebanner-holder -->
                <!-- ============================================== CONTENT : END ============================================== -->
            </div><!-- /.row -->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
            <div id="brands-carousel" class="logo-slider wow fadeInUp">

                <h3 class="section-title">Our Brands</h3>
                <div class="logo-slider-inner">
                    <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                        <div class="item m-t-15">
                            <a href="#" class="image">
                                <img data-echo="images/brands/ampeg-logo-130x100.jpg"
                                     src="images/brands/ampeg-logo-130x100.jpg" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item m-t-10">
                            <a href="#" class="image">
                                <img data-echo="images/brands/antari-logo-130x100.png"
                                     src="images/brands/antari-logo-130x100.png" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="images/brands/lewitt-logo-130x100.jpg"
                                     src="images/brands/lewitt-logo-130x100.jpg" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="images/brands/logo_AUDIOCENTER-130x100.png"
                                     src="images/brands/logo_AUDIOCENTER-130x100.png" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="images/brands/pioneer-130x100.gif"
                                     src="images/brands/pioneer-130x100.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="images/brands/stagg_logo-130x100.jpg"
                                     src="images/brands/stagg_logo-130x100.jpg" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="images/brands/yamaha-130x100.png" src="images/brands/yamaha-130x100.png"
                                     alt="">
                            </a>
                        </div><!--/.item-->
                    </div><!-- /.owl-carousel #logo-slider -->
                </div><!-- /.logo-slider-inner -->

            </div><!-- /.logo-slider -->
            <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
        </div><!-- /.container -->
    </div><!-- /#top-banner-and-menu -->

@endsection
